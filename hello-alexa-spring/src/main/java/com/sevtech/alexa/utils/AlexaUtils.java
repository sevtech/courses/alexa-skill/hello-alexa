package com.sevtech.alexa.utils;


import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Random;
import java.util.TimeZone;

import com.amazon.speech.speechlet.Session;
import com.amazon.speech.speechlet.SpeechletResponse;
import com.amazon.speech.ui.Card;
import com.amazon.speech.ui.PlainTextOutputSpeech;
import com.amazon.speech.ui.Reprompt;
import com.amazon.speech.ui.StandardCard;


public class AlexaUtils {

    protected static final String SESSION_CONVERSATION_FLAG = "conversation";


    private AlexaUtils() {
    }


    public static Card newCard(String cardTitle, String cardText ) {

        StandardCard card = new StandardCard();
        card.setTitle( (cardTitle == null) ? "Skill Sevilla" : cardTitle );
        card.setText(cardText);
        return card;
    }

    public static PlainTextOutputSpeech newSpeech(String speechText, boolean appendRepromptText ) {

        PlainTextOutputSpeech speech = new PlainTextOutputSpeech();
        speech.setText( appendRepromptText ? speechText + "\n\n" + "También puedes probar con más funcionalidades. Di \"Ayuda\" para obtener más información" : speechText);

        return speech;
    }

    public static SpeechletResponse newSpeechletResponse(Card card, PlainTextOutputSpeech speech, Session session, boolean shouldEndSession)  {

        // Say it...
        if ( AlexaUtils.inConversationMode(session) && !shouldEndSession) {
            PlainTextOutputSpeech repromptSpeech = new PlainTextOutputSpeech();
            repromptSpeech.setText("También puedes probar con más funcionalidades. Di \"Ayuda\" para obtener más información");

            Reprompt reprompt = new Reprompt();
            reprompt.setOutputSpeech(repromptSpeech);

            return SpeechletResponse.newAskResponse(speech, reprompt, card);
        }
        else {
            return SpeechletResponse.newTellResponse(speech, card);
        }
    }

    public static void setConversationMode(Session session, boolean conversationMode) {
        if ( conversationMode )
            session.setAttribute(SESSION_CONVERSATION_FLAG, "true");
        else
            session.removeAttribute(SESSION_CONVERSATION_FLAG);
    }

    public static boolean inConversationMode(Session session) {
        return session.getAttribute(SESSION_CONVERSATION_FLAG) != null;
    }
}
