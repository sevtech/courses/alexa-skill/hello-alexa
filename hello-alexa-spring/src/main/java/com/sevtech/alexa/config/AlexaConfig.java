package com.sevtech.alexa.config;
import com.sevtech.alexa.handlers.HandlerSpeechlet;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.servlet.ServletRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.amazon.speech.speechlet.servlet.SpeechletServlet;

@Configuration
public class AlexaConfig {
    @Autowired
    private HandlerSpeechlet handlerSpeechlet;

    @Bean
    public ServletRegistrationBean<SpeechletServlet> registerSpeechletServlet() {

        SpeechletServlet speechletServlet = new SpeechletServlet();
        speechletServlet.setSpeechlet(handlerSpeechlet);

        return new ServletRegistrationBean<>(speechletServlet, "/alexa");
    }
}
