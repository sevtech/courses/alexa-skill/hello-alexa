package com.sevtech.alexa.handlers;

import com.sevtech.alexa.utils.AlexaUtils;
import org.springframework.stereotype.Component;

import com.amazon.speech.slu.Intent;
import com.amazon.speech.speechlet.IntentRequest;
import com.amazon.speech.speechlet.Session;
import com.amazon.speech.speechlet.SpeechletResponse;
import com.amazon.speech.ui.Card;
import com.amazon.speech.ui.PlainTextOutputSpeech;


@Component
public class AmazonStopIntentHandler implements IntentHandler {

    @Override
    public SpeechletResponse handleIntent(Intent intent, IntentRequest request, Session session) {

        String speechText= "Muy bien. ¡Hasta Pronto!";

        Card card = AlexaUtils.newCard("'¡Adiós!'", speechText);
        PlainTextOutputSpeech speech = AlexaUtils.newSpeech(speechText, false);

        return AlexaUtils.newSpeechletResponse(card, speech, session, true);

    }

}