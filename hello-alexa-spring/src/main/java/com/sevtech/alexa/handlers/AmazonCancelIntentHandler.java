package com.sevtech.alexa.handlers;

import com.amazon.speech.slu.Intent;
import com.amazon.speech.speechlet.IntentRequest;
import com.amazon.speech.speechlet.Session;
import com.amazon.speech.speechlet.SpeechletResponse;
import com.amazon.speech.ui.Card;
import com.amazon.speech.ui.PlainTextOutputSpeech;
import com.sevtech.alexa.utils.AlexaUtils;
import org.springframework.stereotype.Component;

@Component
public class AmazonCancelIntentHandler implements IntentHandler{

    @Override
    public SpeechletResponse handleIntent(Intent intent, IntentRequest request, Session session) {

        String speechText= "Gracias por asistir a la formación. ¡Arriba los Brew Eagers!";

        Card card = AlexaUtils.newCard("Fin", speechText);
        PlainTextOutputSpeech speech = AlexaUtils.newSpeech(speechText, false);

        return AlexaUtils.newSpeechletResponse(card, speech, session, true);
    }
}
