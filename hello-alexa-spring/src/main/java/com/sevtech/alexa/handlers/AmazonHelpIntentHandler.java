package com.sevtech.alexa.handlers;

import com.sevtech.alexa.utils.AlexaUtils;
import org.springframework.stereotype.Component;

import com.amazon.speech.slu.Intent;
import com.amazon.speech.speechlet.IntentRequest;
import com.amazon.speech.speechlet.Session;
import com.amazon.speech.speechlet.SpeechletResponse;
import com.amazon.speech.ui.Card;
import com.amazon.speech.ui.PlainTextOutputSpeech;


@Component
public class AmazonHelpIntentHandler implements IntentHandler {

    @Override
    public SpeechletResponse handleIntent(Intent intent, IntentRequest request, Session session) {

        Card card = AlexaUtils.newCard("Ayuda", "¡Puedo saludarte,piropearte y muchas mas cosas!¿Podríamos empezar diciendome como te llamas?");
        PlainTextOutputSpeech speech = AlexaUtils.newSpeech("¡Puedo saludarte,piropearte y muchas mas cosas!¿Podríamos empezar diciendome como te llamas?", false);

        return AlexaUtils.newSpeechletResponse(card, speech, session, false);
    }

}