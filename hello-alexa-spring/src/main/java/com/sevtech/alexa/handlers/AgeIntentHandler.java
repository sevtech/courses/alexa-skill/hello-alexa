package com.sevtech.alexa.handlers;
import com.amazon.speech.slu.Intent;
import com.amazon.speech.slu.Slot;
import com.amazon.speech.speechlet.IntentRequest;
import com.amazon.speech.speechlet.Session;
import com.amazon.speech.speechlet.SpeechletResponse;
import com.amazon.speech.ui.Card;
import com.amazon.speech.ui.PlainTextOutputSpeech;
import com.sevtech.alexa.utils.AlexaUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;

import java.util.Optional;


@Component
public class AgeIntentHandler implements IntentHandler {

    @Override
    public SpeechletResponse handleIntent(Intent intent, IntentRequest request, Session session) {

        Optional<String> ageValue = Optional.ofNullable(intent.getSlot("age").getValue());

        String speechText = ageValue.map(age -> String.format("Vaya, pareces mayor de %S. Lo siento mucho.", age))
                .orElse("No he entendido tu edad. ¿Me lo repites?");

        Card card = AlexaUtils.newCard("age", speechText);
        PlainTextOutputSpeech speech = AlexaUtils.newSpeech(speechText, AlexaUtils.inConversationMode(session));

        return AlexaUtils.newSpeechletResponse( card, speech, session, false);
    }
}
