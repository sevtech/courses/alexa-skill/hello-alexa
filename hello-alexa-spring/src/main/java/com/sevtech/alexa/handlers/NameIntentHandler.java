package com.sevtech.alexa.handlers;

import com.amazon.speech.slu.Intent;
import com.amazon.speech.speechlet.IntentRequest;
import com.amazon.speech.speechlet.Session;
import com.amazon.speech.speechlet.SpeechletResponse;
import com.amazon.speech.ui.Card;
import com.amazon.speech.ui.PlainTextOutputSpeech;
import com.sevtech.alexa.utils.AlexaUtils;
import org.springframework.stereotype.Component;

import java.util.Arrays;
import java.util.Optional;

@Component
public class NameIntentHandler implements IntentHandler {

    public static final String INTENT_NAME = "NameIntent";

    @Override
    public SpeechletResponse handleIntent(Intent intent, IntentRequest request, Session session) {

        String[] brewEagers = {"dani","marco","alberto","jesús","miguel","fran","alex","luis","selu","manuel","nacho"};
        Optional<String> nameValue = Optional.ofNullable(intent.getSlot("name").getValue());
        String message;
        if(Arrays.asList(brewEagers).contains(nameValue.get())){
            message = "¡Hombre! si es %s, parece que tenemos un verdadero Brew Eager aquí. ¿Podrás decirme tu edad, máquina?";
        }else{
            message = "Hola %s, encantada de conocerte. ¿Podrás decirme tu edad?";
        }
        String speechText = nameValue.map(age -> String.format(message, age))
                .orElse("No he entendido tu nombre. ¿Me lo repites?");

        Card card = AlexaUtils.newCard("age", speechText);
        PlainTextOutputSpeech speech = AlexaUtils.newSpeech(speechText, AlexaUtils.inConversationMode(session));

        return AlexaUtils.newSpeechletResponse( card, speech, session, false);
    }
}
