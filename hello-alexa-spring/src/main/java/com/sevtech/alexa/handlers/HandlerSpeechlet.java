package com.sevtech.alexa.handlers;

import com.sevtech.alexa.utils.AlexaUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.amazon.speech.json.SpeechletRequestEnvelope;
import com.amazon.speech.slu.Intent;
import com.amazon.speech.speechlet.IntentRequest;
import com.amazon.speech.speechlet.LaunchRequest;
import com.amazon.speech.speechlet.Session;
import com.amazon.speech.speechlet.SessionEndedRequest;
import com.amazon.speech.speechlet.SessionStartedRequest;
import com.amazon.speech.speechlet.SpeechletResponse;
import com.amazon.speech.speechlet.SpeechletV2;
import com.amazon.speech.ui.Card;
import com.amazon.speech.ui.PlainTextOutputSpeech;


@Service
public class HandlerSpeechlet implements SpeechletV2 {
    protected Logger logger = LoggerFactory.getLogger(HandlerSpeechlet.class);

    @Autowired
    private BeanFactory beanFactory;



    public HandlerSpeechlet() {
    }

    @Override
    public void onSessionStarted(SpeechletRequestEnvelope<SessionStartedRequest> requestEnvelope) {
    }

    @Override
    public SpeechletResponse onLaunch(SpeechletRequestEnvelope<LaunchRequest> requestEnvelope) {

        Session session = requestEnvelope.getSession();
        AlexaUtils.setConversationMode(session, true);

        String speechText = "Hola Bienvenido a la Skill de Sevilla para la formación de los Brew Eagers. ";

        Card card = AlexaUtils.newCard("¡Bienvenido!", speechText);
        PlainTextOutputSpeech speech = AlexaUtils.newSpeech(speechText, false);

        return AlexaUtils.newSpeechletResponse(card, speech, session, false);
    }

    @Override
    public SpeechletResponse onIntent(SpeechletRequestEnvelope<IntentRequest> requestEnvelope) {

        IntentRequest request = requestEnvelope.getRequest();
        Session session = requestEnvelope.getSession();

        Intent intent = request.getIntent();
        if ( intent != null ) {

            String intentName = intent.getName();
            String handlerBeanName = intentName + "Handler";

            handlerBeanName = StringUtils.replace(handlerBeanName, "AMAZON.", "Amazon");
            handlerBeanName = handlerBeanName.substring(0, 1).toLowerCase() + handlerBeanName.substring(1);

            if ( logger.isInfoEnabled() )
                logger.info("About to invoke handler '" + handlerBeanName + "' for intent '" + intentName + "'.");

            // Handle the intent by delegating to the designated handler.
            try {
                Object handlerBean = beanFactory.getBean(handlerBeanName);

                if ( handlerBean instanceof IntentHandler ) {
                    IntentHandler intentHandler = (IntentHandler) handlerBean;
                    return intentHandler.handleIntent(intent, request, session);
                }
            }
            catch (Exception e) {
                logger.error("Error handling intent " + intentName, e);
            }
        }

        AlexaUtils.setConversationMode(session, true);

        String errorText = "No te he entendido";

        Card card = AlexaUtils.newCard("Ni idea", errorText);
        PlainTextOutputSpeech speech = AlexaUtils.newSpeech(errorText, false);

        return AlexaUtils.newSpeechletResponse(card, speech, session, false);
    }

    @Override
    public void onSessionEnded(SpeechletRequestEnvelope<SessionEndedRequest> requestEnvelope) {

    }


}