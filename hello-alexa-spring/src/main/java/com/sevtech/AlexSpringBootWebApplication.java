package com.sevtech;


import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AlexSpringBootWebApplication  {


    public static void main(String[] args) {
        SpringApplication.run(AlexSpringBootWebApplication.class, args);
    }
}