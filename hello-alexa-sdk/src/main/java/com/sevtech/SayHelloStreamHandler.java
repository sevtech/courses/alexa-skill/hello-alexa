package com.sevtech;

import com.amazon.ask.SkillStreamHandler;
import com.amazon.ask.Skills;
import com.sevtech.handler.*;
import com.sevtech.interceptor.LogInterceptor;

public class SayHelloStreamHandler extends SkillStreamHandler {

    public SayHelloStreamHandler() {
        super(Skills.standard()
                .addRequestInterceptor(new LogInterceptor())
                .addResponseInterceptor(new LogInterceptor())
                .addRequestHandlers(
                        new AgeIntentHandler(),
                        new NameIntentHandler(),
                        new NoIntentHandler(),
                        new HelpRequestHandler(),
                        new CustomLaunchRequestHandler())
                .build());
    }

}
