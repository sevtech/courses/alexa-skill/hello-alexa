package com.sevtech.interceptor;

import com.amazon.ask.dispatcher.request.handler.HandlerInput;
import com.amazon.ask.dispatcher.request.interceptor.RequestInterceptor;
import com.amazon.ask.dispatcher.request.interceptor.ResponseInterceptor;
import com.amazon.ask.model.Response;
import com.amazon.ask.util.JacksonSerializer;

import java.util.Optional;


public class LogInterceptor implements RequestInterceptor, ResponseInterceptor {

    @Override
    public void process(HandlerInput handlerInput) {
        System.out.println(new JacksonSerializer().serialize(handlerInput.getRequest()));
    }

    @Override
    public void process(HandlerInput handlerInput, Optional<Response> response) {
        response.ifPresent(res -> System.out.println(new JacksonSerializer().serialize(res)));
    }
}
