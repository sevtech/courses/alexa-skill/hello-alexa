package com.sevtech.handler;

import com.amazon.ask.dispatcher.request.handler.HandlerInput;
import com.amazon.ask.dispatcher.request.handler.RequestHandler;
import com.amazon.ask.model.Response;
import com.amazon.ask.request.RequestHelper;
import com.sevtech.messages.Messages;

import java.util.Optional;

import static com.amazon.ask.request.Predicates.intentName;

public class NameIntentHandler implements RequestHandler {

    public static final String INTENT_NAME = "NameIntent";

    @Override
    public boolean canHandle(HandlerInput handlerInput) {
        return handlerInput.matches(intentName(INTENT_NAME));
    }

    @Override
    public Optional<Response> handle(HandlerInput handlerInput) {
        RequestHelper request = RequestHelper.forHandlerInput(handlerInput);
        Optional<String> name = request.getSlotValue("name");
        final String text = name.map(x -> String.format(Messages.NAME.getText(), x))
                .orElse(Messages.NONAME.getText());

        return handlerInput.getResponseBuilder()
                .withSpeech(text)
                .withShouldEndSession(false)
                .build();

    }
}
