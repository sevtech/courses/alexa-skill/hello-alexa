package com.sevtech.handler;

import com.amazon.ask.dispatcher.request.handler.HandlerInput;
import com.amazon.ask.dispatcher.request.handler.RequestHandler;
import com.amazon.ask.model.Response;
import com.sevtech.messages.Messages;

import java.util.Optional;

import static com.amazon.ask.request.Predicates.intentName;

public class HelpRequestHandler implements RequestHandler {

    private static final String INTENT_NAME = "AMAZON.HelpIntent";

    @Override
    public boolean canHandle(HandlerInput handlerInput) {
        return handlerInput.matches(intentName(INTENT_NAME));
    }

    @Override
    public Optional<Response> handle(HandlerInput handlerInput) {
        return handlerInput.getResponseBuilder()
                .withSpeech(Messages.HELP.getText())
                .withReprompt(Messages.HELP.getText())
                .withShouldEndSession(false)
                .build();
    }
}
