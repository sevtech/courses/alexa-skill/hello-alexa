package com.sevtech.handler;

import com.amazon.ask.dispatcher.request.handler.HandlerInput;
import com.amazon.ask.dispatcher.request.handler.RequestHandler;
import com.amazon.ask.model.Response;
import com.sevtech.messages.Messages;

import java.util.Optional;

import static com.amazon.ask.request.Predicates.intentName;

public class NoIntentHandler implements RequestHandler {

    private static final String NOINTENT_NAME = "AMAZON.NoIntent";
    private static final String STOP_INTENT_NAME = "AMAZON.StopIntent";
    private static final String CANCEL_INTENT_NAME = "AMAZON.CancelIntent";


    @Override
    public boolean canHandle(HandlerInput handlerInput) {
        return handlerInput.matches(intentName(NOINTENT_NAME).or(intentName(STOP_INTENT_NAME).or(intentName(CANCEL_INTENT_NAME))));
    }

    @Override
    public Optional<Response> handle(HandlerInput handlerInput) {
        return handlerInput.getResponseBuilder()
                .withSpeech(Messages.THANKS.getText())
                .withShouldEndSession(true)
                .build();
    }
}
