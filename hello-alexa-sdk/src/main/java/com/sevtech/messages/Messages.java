package com.sevtech.messages;

public enum Messages {
    WELCOME("¡Bienvenido a la primera skill realizada por SevTech! Pregúntame que puedo hacer para descubrirme"),
    HELP("¡Puedo saludarte,piropearte y muchas mas cosas!¿Podríamos empezar diciendome como te llamas?"),
    THANKS("Gracias por usar nuestra aplicación"),
    NONAME("No he entendido tu nombre. ¿Me lo repites?"),
    NAME("Hola %s, encantada de conocerte. ¿Te atreves a decirme la edad?"),
    AGE("Vaya, pareces mayor de %S. Lo siento mucho,"),
    NOAGE("No he entendido tu edad. ¿Me lo repites?");

    private String text;

    public String getText() {
        return text;
    }

    Messages(String text) {
        this.text = text;
    }
}
