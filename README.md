# hello-alexa

Basic project where we will introduce a conversation, the use of locations and saving local data

# Built in 🛠
- Java: JDK 1.8

# Requirements 📋

- Create a fork from this repository.
- Create AWS account to deploy sdk project al AWS Lambda.
- It's necessary register your Amazon account at Amazon Developer Portal (https://developer.amazon.com/alexa/console/ask).                                               
- Recommended Ngrok (or similar) for HTTPS solution testing at local.

# Deploy Tips 📦
- Ngrok deploy command: ngrok http 8080 -region=eu

# Developers ⌨
- [Jesús Jiménez](https://www.linkedin.com/in/jesjimher/)
- [Daniel Curiel](https://www.linkedin.com/in/curieldaniel7/)